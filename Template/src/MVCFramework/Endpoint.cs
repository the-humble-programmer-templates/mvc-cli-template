using System;
using System.Collections.Generic;
using System.Linq;
using Kirinnee.Helper;

namespace HumbleProgrammer.MVCFramework
{
    public class Endpoint : Attribute
    {
        private string _route;

        public Endpoint(string route)
        {
            _route = route;
        }

        public (bool, Dictionary<string, string>) Match(string[] input)
        {
            var items = this._route.SplitBy(" ").ToArray();

            if (input.Length > items.Length) return (false, null);
            var ret = new Dictionary<string, string>();
            var matches = 0;
            for (var i = 0; i < items.Length; i++)
            {
                var curr = items[i];
                if (i < input.Length)
                {
                    if (curr == input[i])
                        matches++;
                    else if (curr.StartsWith("{") && curr.EndsWith("}"))
                    {
                        matches++;
                        ret.Add(curr.Skip(1).Omit(1), input[i]);
                    }
                    else
                    {
                        return (false, null);
                    }
                }
                else
                {
                    if (curr.StartsWith("{") && curr.EndsWith("}"))
                    {
                    }
                    else
                    {
                        return (false, null);
                    }
                }
            }

            return matches == input.Length ? (true, ret) : (false, null);
        }
    }
}