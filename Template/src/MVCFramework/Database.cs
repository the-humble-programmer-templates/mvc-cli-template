using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace HumbleProgrammer.MVCFramework
{
    public class Database
    {
        public void Save(string table, string key, object a)
        {
            var data = JsonConvert.SerializeObject(a);
            File.WriteAllText($"./database/{table}_{key}", data);
            UpdateIndex(table, key, true);
        }

        public string[] TableKeys(string table)
        {
            return LoadIndex(table).Where((k) => k.Value).Select(v => v.Key).ToArray();
        }

        public T Load<T>(string table, string key) where T : class
        {
            if (Exist(table, key))
            {
                var s = File.ReadAllText($"./database/{table}_{key}");
                return JsonConvert.DeserializeObject<T>(s);
            }

            return null;
        }

        public void Delete(string table, string key)
        {
            File.Delete($"./database/{table}_{key}");
            UpdateIndex(table, key, false);
        }

        public bool Exist(string table, string key)
        {
            var index = LoadIndex(table);
            return index.ContainsKey(key) && index[key];
        }

        private void UpdateIndex(string table, string key, bool val)
        {
            var x = LoadIndex(table);
            x[key] = val;
            SaveIndex(table, x);
        }

        private void SaveIndex(string table, Dictionary<string, bool> indexes)
        {
            var val = JsonConvert.SerializeObject(indexes);
            File.WriteAllText("./database/" + table, val);
        }

        private Dictionary<string, bool> LoadIndex(string table)
        {
            var indexes = File.Exists("./database/" + table) ? File.ReadAllText("./database/" + table) : "{}";
            return JsonConvert.DeserializeObject<Dictionary<string, bool>>(indexes);
        }
    }
}