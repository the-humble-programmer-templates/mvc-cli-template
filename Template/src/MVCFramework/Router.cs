using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Kirinnee.Helper;

namespace HumbleProgrammer.MVCFramework
{
    public class Router
    {
        private object[] _controllers;

        public Router(object[] controllers)
        {
            _controllers = controllers;
        }

        public void Route(string input)
        {
            Route(input.SplitBy(" ").ToArray());
        }

        public (bool, string) Route(string[] input)
        {
            var (o, io) = FindClass(input);
            if (o == null) return (false, "no controller match");
            var (method, var) = FindMethod(io, o);
            if (method == null) return (false, "no method match");

            var para = method.GetParameters();
            var max = para.Length;
            var min = para.Count(x => !x.IsOptional && !x.HasDefaultValue);
            if (var.Count < min && var.Count > max) return (false, "not enough parameters");
            var param = new object[para.Length];
            var pArr = para.ToArray();
            var count = 0;
            for (var i = 0; i < max; i++)
            {
                var name = pArr[i].Name;
                if (var.ContainsKey(name))
                {
                    param[pArr[i].Position] = var[name];
                    count++;
                }
                else if (i < min)
                {
                    return (false, "missing parameter: " + name);
                }
                else
                {
                    param[pArr[i].Position] = Type.Missing;
                }
            }

            if (count < min) return (false, "not enough parameters");
            method.Invoke(o, param);
            return (true, "");
        }

        public (MethodInfo, Dictionary<string, string>) FindMethod(string[] input, object o)
        {
            // Iterate through all the methods of the class.
            foreach (var mInfo in o.GetType().GetMethods())
            {
                // Iterate through all the Attributes for each method.
                foreach (var attr in Attribute.GetCustomAttributes(mInfo))
                {
                    // Check for the AnimalType attribute.
                    if (attr.GetType() == typeof(Endpoint))
                    {
                        if (attr is Endpoint att)
                        {
                            var (match, variable) = att.Match(input);
                            if (match)
                            {
                                return (mInfo, variable);
                            }
                        }
                    }
                }
            }

            return (null, null);
        }

        public (object, string[]) FindClass(string[] input)
        {
            foreach (var o in this._controllers)
            {
                var (match, leftover) = MatchClass(o, input);
                if (match)
                {
                    return (o, leftover);
                }
            }

            return (null, null);
        }

        public (bool, string[]) MatchClass(object o, string[] input)
        {
            foreach (var attr in Attribute.GetCustomAttributes(o.GetType()))
            {
                if (attr.GetType() != typeof(Controller)) continue;
                if (attr is Controller att && att.Match(input))
                {
                    return (true, att.Leftover(input));
                }
            }

            return (false, new[] {""});
        }
    }
}