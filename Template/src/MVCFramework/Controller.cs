using System;
using System.Linq;
using Kirinnee.Helper;

namespace HumbleProgrammer.MVCFramework
{
    public class Controller : Attribute
    {
        private string _route;

        public Controller(string route)
        {
            _route = route;
        }

        public string[] Leftover(string[] input)
        {
            return input.Skip(this._route.SplitBy(" ").ToArray().Length).ToArray();
        }

        public bool Match(string[] input)
        {
            var x = _route.SplitBy(" ").ToArray();
            if (x.Length > input.Length)
            {
                return false;
            }

            return !x.Where((curr, i) => curr != input[i]).Any();
        }
    }
}