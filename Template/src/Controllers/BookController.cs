using System;
using HumbleProgrammer.Models;
using HumbleProgrammer.MVCFramework;
using HumbleProgrammer.Views;

namespace HumbleProgrammer.Controllers
{
    [Controller("book")]
    public class BookController
    {
        private BookView _view = new BookView();

        [Endpoint("list")]
        public void List()
        {
            var book1 = new Book("Eric Evans", "Domain Driven Design");
            var book2 = new Book("Bob Martin", "Clean Code");
            var book3 = new Book("JK Rowling", "Harry Potter");
            var books = new[] {book1, book2, book3};

            _view.Render(books);
        }
    }
}