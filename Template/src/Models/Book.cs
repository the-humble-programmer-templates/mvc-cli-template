namespace HumbleProgrammer.Models
{
    public class Book
    {
        public string Author;
        public string Title;

        public Book(string author, string title)
        {
            Author = author;
            Title = title;
        }

        public Book()
        {
        }
    }
}