using System;
using System.Net;
using HumbleProgrammer.Models;

namespace HumbleProgrammer.Views
{
    public class BookView
    {
        public void Render(Book[] books)
        {
            foreach (var book in books)
            {
                Console.WriteLine(book.Title + " by " + book.Author);
            }
        }
    }
}