﻿using System;
using HumbleProgrammer.Controllers;
using HumbleProgrammer.MVCFramework;

namespace HumbleProgrammer
{
    class Program
    {
        static void Main(string[] args)
        {
            //Controller registration
            var bookController = new BookController();

            // Routing
            var router = new Router(new object[] {bookController});
            var (success, message) = router.Route(args);
            if (!success)
            {
                Console.WriteLine(message);
            }
        }
    }
}